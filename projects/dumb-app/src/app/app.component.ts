import { Component } from '@angular/core';
import { MenuOptions, MenuParams, SimpleMenuService } from 'simple-menu';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dumb-app';

  constructor(private themes: SimpleMenuService) {}

  options: MenuOptions = {
    menuFixed: true,
    logo: './../../assets/logo_pvt.png',
    theme: this.themes.classic
  }

  params: MenuParams[] = [
    {
      label: "Link param 1",
      link: {
        route: "/link3"
      },
      active: true
    },
    {
      label: "Link param 2",
      subOpen: false,
      subMenu: [
        {
          label: "Link 2.a",
          link: {
            route: "/link3"
          },
        },
        {
          label: "Link 2.b",
          link: {
            route: "https://www.google.com",
            external: true
          },
        }
      ]
    }
  ];

}
