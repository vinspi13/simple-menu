import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SimpleMenuService } from '../simple-menu.service';

export interface MenuTheme {
  primaryBackgroundColor: String,
  primaryColor: String,
  secondaryBackgroundColor: String,
  secondaryColor: String
}

export interface MenuOptions {
  menuFixed: Boolean,
  theme?: MenuTheme,
  logo?: String
}

export interface MenuParams {
  label: String,
  link?: {
    route: String,
    external?: Boolean
  },
  subMenu?: MenuParams[],
  active?: Boolean,
  subOpen?: Boolean
}



@Component({
  selector: 'simple-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  toggleMenu: Boolean = false;

  @Input('params')
  params: MenuParams[];

  
  _options: MenuOptions = {
    menuFixed: false,

  }  

  @Input('options')
  set options(opt: MenuOptions) {

    this._options = opt;
    if(opt.theme == null) {
      this._options.theme = this.themes.classic;
    }

  }

  constructor(
    private router: Router,
    private themes: SimpleMenuService
    ) { }

  ngOnInit(): void {

  }

  routing(item: MenuParams) {
    
    if(item.link != null) {
      if(item.link.external) {
        console.log('external URL');
        
        window.location.href = item.link.route.toString();
      }
      this.router.navigate([item.link.route]);
    }
  }

  clickOnItem(item: MenuParams) {
    
    var state = item.subOpen;

    this.params.forEach(i => {
      i.active = false;
      i.subOpen = false;
    });

    item.subOpen = !state;
    item.active = true;

    

  }

  toggleMenuAction() {
    this.toggleMenu = !this.toggleMenu;
  }

  hoverMouseenterLink(event) {
    if(!event.srcElement.classList.contains('active')) {
      event.srcElement.style.backgroundColor = this._options.theme.secondaryBackgroundColor;
      event.srcElement.style.color = this._options.theme.secondaryColor;
    }
  }

  hoverMouseleaveLink(event) {
    if(!event.srcElement.classList.contains('active')) {
      event.srcElement.style.backgroundColor = this._options.theme.primaryBackgroundColor;
      event.srcElement.style.color = this._options.theme.primaryColor;
    }
  }

}
