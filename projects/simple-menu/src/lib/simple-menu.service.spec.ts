import { TestBed } from '@angular/core/testing';

import { SimpleMenuService } from './simple-menu.service';

describe('SimpleMenuService', () => {
  let service: SimpleMenuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SimpleMenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
