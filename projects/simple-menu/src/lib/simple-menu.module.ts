import { NgModule } from '@angular/core';
import { SimpleMenuComponent } from './simple-menu.component';
import { MenuComponent } from './menu/menu.component';
import { BrowserModule } from '@angular/platform-browser';



@NgModule({
  declarations: [SimpleMenuComponent, MenuComponent],
  imports: [
    BrowserModule
  ],
  exports: [SimpleMenuComponent, MenuComponent]
})
export class SimpleMenuModule { }
