import { Component, OnInit } from '@angular/core';
import { MenuTheme } from 'dist/mg-responsive-menu/public-api';

@Component({
  selector: 'lib-simple-menu',
  template: `
    <p>
      simple-menu works!
    </p>
  `,
  styles: [
  ]
})
export class SimpleMenuComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
