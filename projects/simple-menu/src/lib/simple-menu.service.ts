import { Injectable } from '@angular/core';
import { MenuTheme } from './menu/menu.component';

@Injectable({
  providedIn: 'root'
})
export class SimpleMenuService {

  public classic: MenuTheme = {
    primaryBackgroundColor: '#f3f3f3',
    primaryColor: 'black',
    secondaryBackgroundColor: '#525D8E',
    secondaryColor: 'white'
  }    

  public dark: MenuTheme = {
      primaryBackgroundColor: '#34495e',
      primaryColor: 'white',
      secondaryBackgroundColor: '#34495e',
      secondaryColor: '#d35400'
  }

  public darkGreen: MenuTheme = {
      primaryBackgroundColor: '#34495e',
      primaryColor: 'white',
      secondaryBackgroundColor: '#16a085',
      secondaryColor: 'white'
  }

  public lavand: MenuTheme = {
      primaryBackgroundColor: '#7887AB',
      primaryColor: 'white',
      secondaryBackgroundColor: '#2E4172',
      secondaryColor: 'white'
  }

  public purple: MenuTheme = {
      primaryBackgroundColor: '#8D7AA8',
      primaryColor: 'white',
      secondaryBackgroundColor: '#6A4F8E',
      secondaryColor: 'white'
  }

  public pinky: MenuTheme = {
      primaryBackgroundColor: '#F3AAAE',
      primaryColor: 'white',
      secondaryBackgroundColor: '#CD6A6F',
      secondaryColor: 'white'
  }

  public autumn: MenuTheme = {
      primaryBackgroundColor: '#D18F6C',
      primaryColor: 'white',
      secondaryBackgroundColor: '#AA6039',
      secondaryColor: 'white'
  }

  constructor() { }
}
