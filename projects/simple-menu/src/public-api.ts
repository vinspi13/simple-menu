/*
 * Public API Surface of simple-menu
 */

export * from './lib/simple-menu.service';
export * from './lib/simple-menu.component';
export * from './lib/simple-menu.module';

export * from './lib/menu/menu.component';

